package jTiled2D.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Esta classe gerencia inserção de dados via teclado
 */
public class KeyManager implements KeyListener {

    private boolean[] keys = new boolean[256];
    public boolean cima, baixo, esquerda, direita, confirma, cancela;

    public KeyManager() {
        cima = keys[KeyEvent.VK_UP];
        baixo = keys[KeyEvent.VK_DOWN];
        esquerda = keys[KeyEvent.VK_LEFT];
        direita = keys[KeyEvent.VK_RIGHT];
        confirma = keys[KeyEvent.VK_ENTER];
        cancela = keys[KeyEvent.VK_ESCAPE];
    }

    /**
     * Cada tick atualiza as keys
     */
    public void tick() {
        cima = keys[KeyEvent.VK_UP];
        baixo = keys[KeyEvent.VK_DOWN];
        esquerda = keys[KeyEvent.VK_LEFT];
        direita = keys[KeyEvent.VK_RIGHT];
        confirma = keys[KeyEvent.VK_ENTER];
        cancela = keys[KeyEvent.VK_ESCAPE];
    }

	@Override
    public void keyPressed(KeyEvent e) {
        keys[e.getKeyCode()] = true;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        keys[e.getKeyCode()] = false;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        //Não faz nada aqui
    }
}
