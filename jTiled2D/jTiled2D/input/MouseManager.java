
package jTiled2D.input;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import javax.swing.event.MouseInputListener;

/**
 * Esta classe captura os eventos de Mouse
 */
public class MouseManager implements MouseInputListener, MouseMotionListener {

    protected boolean clickEsq, clickDir;
    protected int mouseX, mouseY;
    
    public MouseManager() {
    }

    
    @Override
    public void mouseClicked(MouseEvent e) {
        
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if(e.getButton() == MouseEvent.BUTTON1)
            clickEsq = true;
        if(e.getButton() == MouseEvent.BUTTON3)
            clickDir = true;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if(e.getButton() == MouseEvent.BUTTON1)
            clickEsq = false;
        if(e.getButton() == MouseEvent.BUTTON3)
            clickDir = false;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        mouseX = e.getX();
        mouseY = e.getY();
    }

    public boolean isClickEsq() {
        return clickEsq;
    }

    public boolean isClickDir() {
        return clickDir;
    }

    public int getMouseX() {
        return mouseX;
    }

    public int getMouseY() {
        return mouseY;
    }
}
