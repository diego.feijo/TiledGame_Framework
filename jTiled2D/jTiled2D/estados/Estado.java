package jTiled2D.estados;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import jTiled2D.Config;


public abstract class Estado {
    
    private static Estado estadoAtual = null;
    //Background
    private BufferedImage planoFundo;

    public abstract void init();

    public abstract void tick();

    public abstract void render(Graphics g);

    public void desenhaPlanoFundo(Graphics g){
            if (planoFundo != null) {
                int w = Config.getInstance().getLarguraJanela();
                int h = Config.getInstance().getAlturaJanela();
                int iw = planoFundo.getWidth();
                int ih = planoFundo.getHeight();
                int x = 0;
                int y = 0;
                while (y < h) {
                    while (x < w) {
                        g.drawImage(planoFundo, x, y, null);
                        x += iw;
                    }
                    y += ih;
                    x = 0;
                }
            }
    }
    
    public static void setEstado(Estado estado) {
        estadoAtual = estado;
    }

    public static Estado getEstadoAtual() {
        return estadoAtual;
    }
}
