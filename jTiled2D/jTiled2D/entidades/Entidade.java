package jTiled2D.entidades;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import jTiled2D.Config;
import jTiled2D.excecoes.MapaInvalidoException;
import jTiled2D.excecoes.TileException;
import jTiled2D.gfx.Animacao;
import jTiled2D.gfx.EntidadeSprite;
import jTiled2D.input.KeyManager;
import jTiled2D.mapas.Mapa;
import jTiled2D.tiles.Tile;

/**
 * Entidades são atores no jogo. Podem representar inimigos, personagens não jogáveis
 * e até mesmo o próprio jogador.
 */
public abstract class Entidade {

    //Controladores de movimento
    public boolean movendo, ativo, solido;
    public Direcao dir;
    public float x, y, xMove, yMove;
    //Controladores de acao
    public boolean acaoDisparada, acaoCancelada;
    //Dimensões
    protected int largura, altura;
    protected int deltaX, deltaY;
    protected int alturaTile, larguraTile;
    //Colisão
    protected Rectangle limites;
    protected float velocidade;
    // Animacao
    protected Animacao[] animacoes;
    //Mapa atual
    protected Mapa mapa;
    protected KeyManager km;

    /**
     * Cria uma entidade no jogo. O limite padrão é um retângulo com uma área
     * 3 px menor que a imagem da entidade.
     *
     * @param x              a posição x, em pixels, no mapa
     * @param y              a posição y, em pixels, no mapa
     * @param entidadeSprite o sprite da Entidade
     */
    public Entidade(float x, float y, EntidadeSprite entidadeSprite) throws Exception {
        if (entidadeSprite == null) throw new IllegalArgumentException("entidadeSprite não pode ser null!");

        this.animacoes = entidadeSprite.getAnimacoes();
        this.altura = animacoes[0].getFrameAtual().getHeight();
        this.largura = animacoes[0].getFrameAtual().getWidth();
        this.x = x;
        this.y = y;
        this.ativo = true;
        this.solido = true;
        init();
    }

    private void init() throws Exception {
        limites = new Rectangle(3, 3, this.largura - 6, this.altura - 6);
        movendo = false;
        dir = Direcao.PARADO;
        Config config = Config.getInstance();
        this.alturaTile = config.getAlturaTile();
        this.larguraTile = config.getLarguraTile();
        this.mapa = config.getMapaAtual();
        this.km = config.getKeyManager();

        //Validações
        if (alturaTile <= 0 || larguraTile <= 0) throw new TileException();
        if (mapa == null) throw new MapaInvalidoException();
    }

    /**
     * Atualiza a entidade
     */
    public void tick() {
        //Isso faz com que o movimento seja suave
        if (velocidade <= 0) {
            velocidade = larguraTile / 8;
        }
        // Animações
        for (Animacao a : animacoes) {
            a.tick();
        }
        //Movimentos
        if (movendo) {
            mova();
        }

        if (acaoDisparada)
            disparaAcao();

        if (acaoCancelada)
            cancelaAcao();
    }

    protected void disparaAcao() {};

    protected void cancelaAcao() {};

    /**
     * Move a entidade suavemente, um tile por evento keyPressed.
     */
    private void mova() {
        switch (dir) {
            case CIMA:
                if (this.y > deltaY) {
                    yMove = -velocidade;
                    moveY();
                } else {
                    pararMovimento();
                }
                break;
            case BAIXO:
                if (this.y < deltaY) {
                    yMove = velocidade;
                    moveY();
                } else {
                    pararMovimento();
                }
                break;
            case ESQUERDA:
                if (this.x > deltaX) {
                    xMove = -velocidade;
                    moveX();
                } else {
                    pararMovimento();
                }
                break;
            case DIREITA:
                if (this.x < deltaX) {
                    xMove = velocidade;
                    moveX();
                } else {
                    pararMovimento();
                }
                break;
            case PARADO:
                break;
        }

    }

    private void moveX() {
        if (xMove > 0) { //Move para direita
            int tx = (int) (x + xMove + limites.x + limites.width) / larguraTile;
            if (!colisaoComTile(tx, (int) (y + limites.y) / alturaTile)
                    && !colisaoComTile(tx, (int) (y + limites.y + limites.height) / alturaTile)) {
                x += xMove;
            } else {
                x = tx * larguraTile - limites.x - limites.width - 1;
            }
        } else if (xMove < 0) { // Move para esquerda
            int tx = (int) (x + xMove + limites.x) / larguraTile;
            if (!colisaoComTile(tx, (int) (y + limites.y) / alturaTile)
                    && !colisaoComTile(tx, (int) (y + limites.y + limites.height) / alturaTile)) {
                x += xMove;
            } else {
                x = tx * larguraTile + larguraTile - limites.x;
            }
        }

    }

    private void moveY() {
        if (yMove < 0) {// Move para cima
            int ty = (int) (y + yMove + limites.y) / alturaTile;
            if (!colisaoComTile((int) (x + limites.x) / larguraTile, ty)
                    && !colisaoComTile((int) (x + limites.x + limites.width) / larguraTile, ty)) {
                y += yMove;
            } else {
                y = ty * alturaTile + alturaTile - limites.y;
            }

        } else if (yMove > 0) {//Move para baixo
            int ty = (int) (y + yMove + limites.y + limites.height) / alturaTile;
            if (!colisaoComTile((int) (x + limites.x) / larguraTile, ty)
                    && !colisaoComTile((int) (x + limites.x + limites.width) / larguraTile, ty)) {
                y += yMove;
            } else {
                y = ty * alturaTile - limites.y - limites.height - 1;
            }
        }
    }

    public void moverPara(Direcao direcao) {
        int tileX = (int) x / larguraTile;
        int tileY = (int) y / larguraTile;
        switch (direcao) {
            case CIMA:
                if (!movendo && !colisao(tileX, tileY - 1)) {
                    movendo = true;
                    deltaY = (int) this.y - alturaTile;
                    deltaX = (int) this.x;
                    dir = Direcao.CIMA;
                }
                break;
            case BAIXO:
                if (!movendo && !colisao(tileX, tileY + 1)) {
                    movendo = true;
                    deltaY = (int) this.y + alturaTile;
                    deltaX = (int) this.x;
                    dir = Direcao.BAIXO;
                }
                break;
            case ESQUERDA:
                if (!movendo && !colisao(tileX - 1, tileY)) {
                    movendo = true;
                    deltaY = (int) this.y;
                    deltaX = (int) this.x - larguraTile;
                    dir = Direcao.ESQUERDA;
                }
                break;
            case DIREITA:
                if (!movendo && !colisao(tileX + 1, tileY)) {
                    movendo = true;
                    deltaY = (int) this.y;
                    deltaX = (int) this.x + larguraTile;
                    dir = Direcao.DIREITA;
                }
                break;
        }
        movendo = true;
    }

    protected void moverAleatorio() {
        //Gera um numero aleatorio entre 0 e 1
        double rdn = Math.random();
        Direcao direcao = Direcao.DIREITA;
        if (rdn <= 0.25)
            direcao = Direcao.BAIXO;
        else if (rdn <= 0.5)
            direcao = Direcao.CIMA;
        else if (rdn <= 0.75)
            direcao = Direcao.ESQUERDA;
        moverPara(direcao);
    }

    protected boolean colisao(int x, int y) {
        return colisaoComTile(x, y) || colisaoComEntidade(x, y);
    }

    /**
     * Informa se o tile na posicao (em tiles) e solido
     */
    protected boolean colisaoComTile(int x, int y) {
        if (mapa == null) return false;
        Tile tile = mapa.getTile(x, y);
        return tile != null && tile.isSolido();
    }

    protected boolean colisaoComEntidade(int x, int y) {
        if (mapa == null) return false;
        Entidade e = mapa.getEntity(x, y);
        return e != null && e.isSolido();
    }

    protected void pararMovimento() {
        if (movendo) {
            movendo = false;
            moveu();
        }
    }

    /**
     * Sobrescreva este método para indicar o que acontece ao fim do movimento da entidade
     */
    protected void moveu() {

    };

    /**
     * Quando uma tecla funcional é pressionada (como as setas), este método
     * muda o estado da entidade para "movendo" e para a sua direção equivalente.
     * A entidade não pode mover para outra direção enquanto move.
     * A colisão é conferida antes de mudar a posição.
     * A entidade entao move um tile e para no fim do movimento.
     */
    public void getInputStepByStep() {
        xMove = 0;
        yMove = 0;
        int tileX = (int) x / larguraTile;
        int tileY = (int) y / larguraTile;

        if (km.cima) {
            System.out.println("CIMA");
            if (dir == Direcao.PARADO && !colisaoComTile(tileX, tileY - 1)) {
                movendo = true;
                deltaY = (int) this.y - alturaTile;
                deltaX = (int) this.x;
                dir = Direcao.CIMA;
            }

        } else if (km.baixo) {
            if (dir == Direcao.PARADO && !colisaoComTile(tileX, tileY + 1)) {
                movendo = true;
                deltaY = (int) this.y + alturaTile;
                deltaX = (int) this.x;
                dir = Direcao.BAIXO;
            }
        } else if (km.esquerda) {
            if (dir == Direcao.PARADO && !colisaoComTile(tileX - 1, tileY)) {
                movendo = true;
                deltaY = (int) this.y;
                deltaX = (int) this.x - larguraTile;
                dir = Direcao.ESQUERDA;
            }
        } else if (km.direita) {
            if (dir == Direcao.PARADO && !colisaoComTile(tileX + 1, tileY)) {
                movendo = true;
                deltaY = (int) this.y;
                deltaX = (int) this.x + larguraTile;
                dir = Direcao.DIREITA;
            }
        } else {
            if (!movendo) {
                dir = Direcao.PARADO;
            }
        }
    }

    /**
     * Quando uma tecla funcional é pressionada (como as setas), este método
     * muda o estado da entidade para movendo e para a sua direção equivalente.
     * A entidade não pode mover para outra direção enquanto move.
     * A colisão é conferida antes de mudar a posição.
     * A entidade movimenta para a direcao pressionada ate a tecla ser solta.
     */
    public void getInput() {
        xMove = 0;
        yMove = 0;

        if (km.cima) {
            moverPara(Direcao.CIMA);
        } else if (km.baixo) {
            moverPara(Direcao.BAIXO);
        } else if (km.esquerda) {
            moverPara(Direcao.ESQUERDA);
        } else if (km.direita) {
            moverPara(Direcao.DIREITA);
        } else if (km.confirma) {
            if (!movendo)
                disparaAcao();
        } else if (km.cancela) {
            if (!movendo)
                cancelaAcao();
        }
    }


    /**
     * Renderiza os gráficos da entidade na tela
     *
     * @param g: um Graphics para gerenciar a renderização
     */
    public void render(Graphics g) {
        float limiteX = Config.getInstance().getGameCamera().getLimiteX();
        float limiteY = Config.getInstance().getGameCamera().getLimiteY();
        g.drawImage(getFrameAtualAnimacao(), (int) (x - limiteX), (int) (y - limiteY), largura, altura, null);
    }

    /**
     * Retorna a imagem atual da animação.
     */
    private BufferedImage getFrameAtualAnimacao() {
        int animIndex = 0;
        switch (dir) {
            case BAIXO:
                animIndex = 0;
                break;
            case ESQUERDA:
                animIndex = 1;
                break;
            case DIREITA:
                animIndex = 2;
                break;
            case CIMA:
                animIndex = 3;
                break;
        }
        if (animacoes[animIndex] != null) {
            return animacoes[animIndex].getFrameAtual();
        }
        return animacoes[0].getFrameAtual();
    }

    public float getVelocidade() {
        return velocidade;
    }

    /**
     * Retorna a posição X em pixels
     */
    public float getX() {
        return x;
    }

    /**
     * Retorna a posição Y em pixels
     */
    public float getY() {
        return y;
    }

    /**
     * Retorna a posição X em Tiles
     */
    public int getTileX() {
        return (x / larguraTile == 0 ? 1 : (int) x / larguraTile);
    }

    /**
     * Retorna a posição Y em Tiles
     */
    public int getTileY() {
        return (y / alturaTile == 0 ? 1 : (int) y / alturaTile);
    }

    /**
     * Confere se está na posicao {x,y} em Tiles
     */
    public boolean isTilePos(int[] pos) {
        return (getTileX() == pos[0] && getTileY() == pos[1]);
    }

    public int getLargura() {
        return largura;
    }

    public int getAltura() {
        return altura;
    }

    public boolean isSolido() {
        return solido;
    }

    public void setSolido(boolean solido) {
        this.solido = solido;
    }

    public Rectangle getLimites() {
        return limites;
    }

    //Movimentos da entidade
    public enum Direcao {

        CIMA, BAIXO, ESQUERDA, DIREITA, PARADO
    }

}
