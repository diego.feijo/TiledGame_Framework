package jTiled2D.entidades;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import jTiled2D.Config;
import jTiled2D.excecoes.CameraInvalidaException;
import jTiled2D.excecoes.MapaInvalidoException;
import jTiled2D.excecoes.TileException;
import jTiled2D.gfx.GameCamera;
import jTiled2D.mapas.Mapa;
import jTiled2D.tiles.Tile;

public class EntidadeGerenciador {

    private List<Entidade> entidades;
    private int larguraTile, alturaTile;
    private GameCamera camera;
    private Mapa mapa;

    /**
     * Gerencia as entidades do jogo
     *
     * @param entidades : um array de entidades
     */
    public EntidadeGerenciador(List<Entidade> entidades) throws Exception {
        this.entidades = entidades != null ? entidades : new ArrayList<>();
        init();
    }

    private void init() throws Exception{
        Config config = Config.getInstance();
        this.larguraTile = config.getLarguraTile();
        this.alturaTile = config.getAlturaTile();
        this.camera = config.getGameCamera();

        //Validações
        if (this.alturaTile <= 0 || this.larguraTile <= 0) throw new TileException();
        if (this.camera == null) throw new CameraInvalidaException();
    }

    /**
     * Ciclo das entidades gerenciadas
     */
    public void tick() {
        if (entidades != null) {
            for (Entidade e : entidades) {
                if(e.ativo)
                    e.tick();
            }
        }
    }

    /**
     * Renderiza todas as entidades
     *
     * @param g o Graphics para gerenciar a renderização
     */
    public void render(Graphics g) {
        for (Entidade e : entidades) {
            e.render(g);
        }
    }

    /**
     * Renderiza uma entidade em específico
     *
     * @param g o Graphics para gerenciar a renderização
     * @param e a Entidade a renderizar
     */
    public void render(Graphics g, Entidade e) {
        if (e != null) {
            e.render(g);
        }
    }

    public void desativarEntidades(){
        if(!this.entidades.isEmpty()){
            for (Entidade e: entidades) {
                e.ativo = false;
            }
        }
    }

    public void ativarEntidades(){
        if(!this.entidades.isEmpty()){
            for (Entidade e: entidades) {
                e.ativo = true;
            }
        }
    }

    public void desativarAcaoEntidades(){
        if(!this.entidades.isEmpty()){
            for (Entidade e: entidades) {
                e.acaoDisparada = false;
            }
        }
    }

    public void adicionaEntidade(Entidade e) {
        this.entidades.add(e);
    }

    public void removeEntidade(Entidade e) {
        this.entidades.remove(e);
    }
    
    /**
     * Confere se a entidade irá colidir com um Tile Solido
     *
     * @param x: coordenada X do Tile
     * @param y: coordenada Y do Tile
     * @return true se colide
     */
    protected boolean colisaoComTile(int x, int y) throws MapaInvalidoException{
        //localiza o mapa
        if(this.mapa == null) this.mapa = Config.getInstance().getMapaAtual();
        //Se ainda for nulo
        if(this.mapa == null) throw new MapaInvalidoException();
        Tile t = mapa.getTile(x, y);
        return t.isSolido();
    }

    public List<Entidade> getEntidades() {
        return entidades;
    }

}
