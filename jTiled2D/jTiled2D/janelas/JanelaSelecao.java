package jTiled2D.janelas;

import jTiled2D.Config;
import jTiled2D.input.KeyManager;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.HashMap;

public class JanelaSelecao extends JanelaBase {

    private HashMap<Integer, String> opcoes = new HashMap<>();
    private int indice;
    private boolean cursorMov;

    /**
     * Uma janela com itens selecionáveis
     */
    public JanelaSelecao(Dimensao dimensao, ArrayList<String> opcoes) {
        super(dimensao);
        dimensao.setAltura(0);
        int hashKey = 0;
        for (String s : opcoes) {
            this.opcoes.put(hashKey, s);
            hashKey++;
        }
        this.indice = 0;
    }

    public void tick() {
        if (this.isAtivo()) {
            getInput();
        }
    }

    public void render(Graphics g) {
        if (this.isAtivo()) {
            desenhaJanela(g);
            g.setFont(fontePadrao);
            g.setColor(corPadrao);
            //o tamanho da fonte determina a altura da janela
            int tamanhoFonte = g.getFontMetrics().getHeight();
            //teremos opcoes.size()-1 linhas e então as bordas
            dimensao.setAltura(((opcoes.size() - 1) * tamanhoFonte) + borda);
            //o tema tem uma borda, então o cursor ficará ao seu lado
            int cX = dimensao.getPosX() + borda;
            //alinha o cursor na vertical
            int cY = dimensao.getPosY() + borda + (indice * tamanhoFonte);
            g.drawImage(temaJanela.getCursorESQ()[0], cX, cY, borda, borda, null);
            for (int i = 0; i < this.opcoes.size(); i++) {
                // moldura (8) + cursor (8) então o texto
                int sx = dimensao.getPosX() + borda * 2;
                int sy = (dimensao.getPosY() + borda) + (i * tamanhoFonte) + (tamanhoFonte / 2);
                g.drawString(this.opcoes.get(i), sx, sy);
            }
        }
    }

    private void getInput() {
        KeyManager keyManager = Config.getInstance().getKeyManager();

        if (keyManager.cima) {
            if (!cursorMov) {
                cursorMov = true;
                indice = (indice + opcoes.size() - 1) % opcoes.size();
            }

        } else if (keyManager.baixo) {
            if (!cursorMov) {
                cursorMov = true;
                indice = (indice + 1) % opcoes.size();
            }
        } else {
            cursorMov = false;
        }

    }


    public int getIndice() {
        return indice;
    }

    public int getIndiceOpcao(String valor) {
        for (int i = 0; i < this.opcoes.size(); i++) {
            if (this.opcoes.get(i).equalsIgnoreCase(valor)) {
                return i;
            }
        }
        return -1;
    }

    private String getValorOpcao(int key) {
        return this.opcoes.get(key);
    }

    public String getOpcaoAtual() {
        return getValorOpcao(this.indice);
    }
}
