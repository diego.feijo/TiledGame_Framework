package jTiled2D.janelas;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import jTiled2D.Config;
import jTiled2D.input.MouseManager;

public class JanelaBase {

    protected AlinhamentoHorizontal alinhamentoHorizontalJanela;
    protected AlinhamentoVertical alinhamentoVerticalJanela;

    //Atributos da janela
    protected Dimensao dimensao;
    protected String titulo;
    protected int borda;
    protected Color corPadrao, corFundo;
    protected TemaJanela temaJanela;
    protected boolean transparente, visivel, desabilitado, clicado;
    private boolean ativo;
    protected AlinhamentoHorizontal alinhamentoHorizontal;

    //Atributos de texto
    protected Font fontePadrao;

    //Janela de título
    protected JanelaBase janelaTitulo;
    protected boolean transparentFrame = false;

    /**
     * Escreve um texto no centro da janela
     *
     * @param texto       texto a ser escrito
     * @param alinhamento alinhamento do texto
     * @param cursor      indica se deve ou não ser desenhado um cursor à esquerda
     */
    public void escreveTexto(String texto, AlinhamentoHorizontal alinhamento, Graphics g, boolean cursor) {
        //validações
        if (cursor && temaJanela == null)
            throw new NullPointerException("Para desenhar um cursor, um tema de janela deve ser informado!");

        g.setFont(fontePadrao);
        g.setColor(corPadrao);
        FontMetrics fm = g.getFontMetrics();
        int altura = dimensao.getAltura();
        int largura = dimensao.getLargura();
        int x = dimensao.getPosX();
        int y = dimensao.getPosY();
        int tX;
        int tY = (altura / 2) + (fm.getHeight() / 4);
        if (janelaTitulo != null) {
            tY = ((altura + janelaTitulo.getDimensao().getAltura()) / 2) + (fm.getHeight() / 4);
        }

        switch (alinhamento) {
            case CENTRO:
                tX = largura / 2 - fm.stringWidth(texto) / 2;
                break;
            case DIR:
                if (cursor)
                    tX = borda + largura - fm.stringWidth(texto);
                else
                    tX = largura - fm.stringWidth(texto) - borda;
                break;
            default:
                if (cursor)
                    tX = borda * 2;
                else
                    tX = borda;
        }

        if (cursor) {
            int cursorX = x + tX - borda;
            int cursorY = y + tY - (fm.getHeight() / 2);
            int cursorW = temaJanela.getCursorESQ()[0].getWidth();
            g.drawImage(temaJanela.getCursorESQ()[0], cursorX, cursorY, cursorW, cursorW, null);
        }

        g.drawString(texto, x + tX, y + tY);
    }

    /**
     * Desenha uma janela de título
     */
    private void desenhaTitulo(Graphics g) {
        int alturaTitulo = g.getFontMetrics().getHeight();
        Dimensao d = new Dimensao(alturaTitulo + this.borda * 2, dimensao.getLargura(), dimensao.getPosX(), dimensao.getPosY());
        janelaTitulo = new JanelaBase(d);
        janelaTitulo.setBorda(borda);
        janelaTitulo.setTransparente(transparente);
        janelaTitulo.setCorFundo(corFundo);
        janelaTitulo.desenhaJanela(g);
        janelaTitulo.escreveTexto(titulo, AlinhamentoHorizontal.ESQ, g, false);
    }

    /**
     * Desenha uma janela na tela de jogo
     */
    public JanelaBase(Dimensao dimensao) {
        //Validações
        if (dimensao == null) {
            throw new NullPointerException("Dimensão não pode ser nula!");
        }

        this.dimensao = dimensao;

    }

    /**
     * Desenha a janela de acordo com suas dimensões
     */
    public void desenhaJanela(Graphics graphic) {
        //inicia os componentes da janela
        MouseManager mouseManager = Config.getInstance().getMouseManager();

        this.temaJanela = Config.getInstance().getTemaJanela();
        this.fontePadrao = Config.getInstance().getFontePadrao();
        this.corPadrao = Config.getInstance().getCorPadrao();
        this.borda = this.temaJanela != null ? temaJanela.getMoldura()[0].getWidth() : 0;
        this.visivel = true;
//        this.ativo = false;
        //dimensoes
        int x = dimensao.getPosX();
        int y = dimensao.getPosY();
        int largura = dimensao.getLargura();
        int altura = dimensao.getAltura();

        graphic.setFont(fontePadrao);
        graphic.setColor(corPadrao);
        if (visivel) {
            clicado = mouseManager.isClickEsq();
            int iY;
            if (corFundo != null) {
                graphic.drawRect(x, y, largura, altura);
                graphic.setColor(corFundo);
                graphic.fillRect(x, y, largura, altura);
            }
            //Desenha o fundo da janela
            if (!transparente && temaJanela != null) {
                BufferedImage[] bg = temaJanela.getFundo();
                // cima
                iY = y;
                graphic.drawImage(bg[0], x, iY, borda, borda, null);
                graphic.drawImage(bg[1], x + borda, iY, largura - borda * 2, borda, null);
                graphic.drawImage(bg[2], x + (largura - borda), iY, borda, borda, null);
                // meio
                iY = y + borda;
                graphic.drawImage(bg[3], x, iY, borda, altura - borda, null);
                graphic.drawImage(bg[4], x + borda, iY, largura - borda, altura - borda, null);
                graphic.drawImage(bg[5], x + (largura - borda), iY, borda, altura - borda, null);
                // baixo
                iY = y + (altura - borda);
                graphic.drawImage(bg[6], x, iY, borda, borda, null);
                graphic.drawImage(bg[7], x + borda, iY, largura - borda, borda, null);
                graphic.drawImage(bg[8], x + (largura - borda), iY, borda, borda, null);
            }
            //Desenha a borda
            if (!transparentFrame && temaJanela != null) {
                BufferedImage[] frame = temaJanela.getMoldura();
                // cima
                iY = y;
                graphic.drawImage(frame[0], x, iY, borda, borda, null);
                graphic.drawImage(frame[1], x + borda, iY, largura - borda, borda, null);
                graphic.drawImage(frame[2], x + (largura - borda), iY, borda, borda, null);
                // meio
                iY = y + borda;
                graphic.drawImage(frame[3], x, iY, borda, altura - borda, null);
                graphic.drawImage(frame[4], x + borda, iY, largura - borda, altura - borda, null);
                graphic.drawImage(frame[5], x + (largura - borda), iY, borda, altura - borda, null);
                // baixo
                iY = y + (altura - borda);
                graphic.drawImage(frame[6], x, iY, borda, borda, null);
                graphic.drawImage(frame[7], x + borda, iY, largura - borda, borda, null);
                graphic.drawImage(frame[8], x + (largura - borda), iY, borda, borda, null);
            }
            if (titulo != null && !titulo.trim().isEmpty()) {
                desenhaTitulo(graphic);
            }
        }

    }

    public void setAlinhamentoVertical(AlinhamentoVertical av) {
        //se esta nulo, volta ao padrao
        if(av == null)
            av = AlinhamentoVertical.ACIMA;

        this.alinhamentoVerticalJanela = av;
        //dimensoes da janela de jogo
        int altura = Config.getInstance().getAlturaJanela();
        int posY = 0;
        switch (av) {
            case MEIO:
                posY = altura / 2 - this.dimensao.getAltura() / 2;
                break;
            case ABAIXO:
                posY = altura - this.dimensao.getAltura();
                break;
            default:
                break;
        }
        this.dimensao.setPosY(posY);
    }

    /**
     * Escreve um texto contornado
     */
    public void escreveTextoContornado(Graphics g, String texto, int x, int y, Color corContorno, Color corTexto) {
        g.setColor(corContorno);
        int dist = 1;
        g.drawString(texto, x - dist, y - dist);
        g.drawString(texto, x - dist, y + dist);
        g.drawString(texto, x + dist, y - dist);
        g.drawString(texto, x + dist, y + dist);
        g.setColor(corTexto != null ? corTexto : corPadrao);
        g.drawString(texto, x, y);
    }

    public void setAlinhamentoHorizontal(AlinhamentoHorizontal ah) {
        this.alinhamentoHorizontalJanela = ah;
        //dimensoes da janela de jogo
        int largura = Config.getInstance().getLarguraJanela();
        int posX = 0;
        switch (ah) {
            case CENTRO:
                posX = largura / 2 - this.dimensao.getLargura() / 2;
                break;
            case DIR:
                posX = largura - this.dimensao.getLargura();
                break;
        }
        this.dimensao.setPosX(posX);
    }

    public boolean isMouseOver() {
        int mx = Config.getInstance().getMouseManager().getMouseX();
        int my = Config.getInstance().getMouseManager().getMouseY();
        int x = dimensao.getPosX();
        int y = dimensao.getPosY();
        int largura = dimensao.getLargura();
        int altura = dimensao.getAltura();
        return mx > x
                && mx < (x + largura)
                && my > y
                && my < y + altura;
    }

    public boolean isClicado() {
        return Config.getInstance().getMouseManager().isClickEsq() && !desabilitado && !clicado && isMouseOver();
    }

    public boolean isClicadoFora() {
        return Config.getInstance().getMouseManager().isClickEsq() && !desabilitado && !clicado && !isMouseOver();
    }

    public Color getTextColor() {
        return corPadrao;
    }

    public void setTextColor(Color color) {
        this.corPadrao = color;
    }

    public boolean isVisivel() {
        return visivel;
    }

    public void setVisivel(boolean visivel) {
        this.visivel = visivel;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean focus) {
        this.ativo = focus;
    }

    public boolean isDesabilitado() {
        return desabilitado;
    }

    public void setDesabilitado(boolean desabilitado) {
        this.desabilitado = desabilitado;
    }

    public int getBorda() {
        return Config.getInstance().getTemaJanela().getMoldura()[0].getWidth();
    }

    public void setBorda(int borda) {
        this.borda = borda;
    }

    public void setTransparente(boolean transparente) {
        this.transparente = transparente;
    }

    public void setTransparentBorder(boolean transparentBorder) {
        this.transparentFrame = transparentBorder;
    }

    public Color getCorFundo() {
        return corFundo;
    }

    public void setCorFundo(Color corFundo) {
        this.corFundo = corFundo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setCorPadrao(Color corPadrao) {
        this.corPadrao = corPadrao;
    }

    public Dimensao getDimensao() {
        return dimensao;
    }

    public void setDimensao(Dimensao dimensao) {
        this.dimensao = dimensao;
    }

    public enum AlinhamentoHorizontal {

        ESQ, CENTRO, DIR
    }

    public enum AlinhamentoVertical {

        ACIMA,
        MEIO,
        ABAIXO
    }

}
