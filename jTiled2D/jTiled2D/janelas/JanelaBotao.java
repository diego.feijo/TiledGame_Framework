package jTiled2D.janelas;

import java.awt.Color;
import java.awt.Graphics;

public class JanelaBotao extends JanelaBase {

    private String text;

    public JanelaBotao(Dimensao dimensao, String text) {
        super(dimensao);
        this.text = text;        
        this.visivel = true;
    }

    public void render(Graphics g) {
        if (this.visivel) {
            desenhaJanela(g);
            escreveTexto(text, AlinhamentoHorizontal.CENTRO, g, false);
            if (isMouseOver() || desabilitado) {
                this.setTransparente(true);
                this.setCorFundo(Color.GRAY);
            } else {
                this.setTransparente(false);
            }
        }
    }

}
