package jTiled2D.janelas;

public class Dimensao {
    private int altura;
    private int largura;
    private int posX;
    private int posY;

    public Dimensao(int altura, int largura, int posX, int posY) {
        this.altura = altura;
        this.largura = largura;
        this.posX = posX;
        this.posY = posY;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public int getLargura() {
        return largura;
    }

    public void setLargura(int largura) {
        this.largura = largura;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public Dimensao copy() {
        return new Dimensao(altura, largura, posX, posY);
    }
}
