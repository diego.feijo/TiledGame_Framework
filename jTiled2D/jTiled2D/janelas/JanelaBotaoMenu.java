/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jTiled2D.janelas;

import java.awt.Graphics;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 *
 * @author diego
 */
public class JanelaBotaoMenu extends JanelaBase {

    private HashMap<Integer, JanelaBotao> botoes;
    private String[] opcoes;
    private int indice;

    public JanelaBotaoMenu(Dimensao dimensao, String[] opcoes) {
        super(dimensao);
        dimensao.setAltura(0);
        init(opcoes);
    }

    public JanelaBotaoMenu(String[] opcoes) {
        super(new Dimensao(0, 200, 0, 0));
        init(opcoes);
    }

    public void init(String[] opcoes) {
        this.opcoes = opcoes;
        this.botoes = new HashMap<>();
        //hashmap key
        int key = 0;
        //criação dos botoes
        for (String s : opcoes) {
            JanelaBotao btn = new JanelaBotao(dimensao.copy(), s);
            this.botoes.put(key, btn);
            key++;
        }
    }

    public void render(Graphics g) {
        int btnY = dimensao.getPosY();
        int btnH = g.getFontMetrics().getHeight() + this.getBorda() * 2;
        this.dimensao.setAltura((btnH + 2) * botoes.size());
        for (Entry<Integer, JanelaBotao> entry : botoes.entrySet()) {
            //atualiza botoes
            entry.getValue().dimensao.setLargura(dimensao.getLargura());
            entry.getValue().dimensao.setAltura(btnH);
            entry.getValue().dimensao.setPosX(dimensao.getPosX());
            entry.getValue().dimensao.setPosY(btnY);
            entry.getValue().render(g);
            btnY = btnY + entry.getValue().dimensao.getAltura() + 2;
        }
        //this.setAlinhamentoVertical(this.alinhamentoVerticalJanela);
    }

    public int getIndice() {
        for (Entry<Integer, JanelaBotao> entry : botoes.entrySet()) {
            if (entry.getValue().isClicado()) {
                return entry.getKey();
            }
        }
        return -1;
    }     
    
    public void desabilita(int indice, boolean disable){
        botoes.get(indice).desabilitado = disable;
    }


}
