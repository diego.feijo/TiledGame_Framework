package jTiled2D.janelas;

import java.awt.FontMetrics;
import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import jTiled2D.Config;

public class JanelaInserir extends JanelaBase {

    private JPanel jPanel = new JPanel();
    private JFrame jFrame;
    private JTextField input = new JTextField();
    private boolean pipe = false;
    private int pipeTick = 0;
    private String rotulo;
    private JanelaBase janelaRotulo;
    private int larguraRotulo;
    private int larguraTexto;
    private boolean init;

    /**
     * Uma janela para inserir texto
     */
    public JanelaInserir(Dimensao dimensao) {
        super(dimensao);

    }

    private void init() {
        //Ajusta os elementos da janela
        jFrame = Config.getInstance().getGame().getDisplay().getFrame();
        input = new JTextField();
        input.setBounds(dimensao.getPosX(), dimensao.getPosY(), 20, 20);
        input.setEditable(true);
        jPanel.add(input);
        jFrame.add(jPanel);
        larguraTexto = dimensao.getLargura() - borda * 2;
        init = true;
    }

    public void tick() {
        if (isClicado()) {
            this.setAtivo(true);
        }
        if (isClicadoFora()) {
            this.setAtivo(false);
        }
    }

    @Override
    public void desenhaJanela(Graphics g) {
        if (!init)
            init();

        if (visivel) {
            super.desenhaJanela(g);
            if (rotulo != null && !rotulo.trim().isEmpty()) {
                drawLabel(g);
            }
            String stringToDisplay;
            if (input.getText().length() > larguraTexto) {
                input.setText(input.getText().substring(0, larguraTexto));
            }
            String texto = input.getText();
            //Request focus somente se ativo
            if (this.isAtivo()) {
                input.setEnabled(true);
                input.requestFocusInWindow();
                input.setCaretPosition(texto.length());
                stringToDisplay = String.format("%s%s", texto, pipe ? "|" : "");
            } else {
                input.setEnabled(false);
                stringToDisplay = String.format("%s", texto);
            }
            escreveTexto(g, stringToDisplay);
            //simula um cursor piscando
            if (pipeTick == 0) {
                pipe = !pipe;
            }
            pipeTick = (pipeTick + 1) % 30;
        }
    }

    private void escreveTexto(Graphics g, String texto) {
        FontMetrics fm = g.getFontMetrics();
        int tX;
        if (larguraRotulo > 0) {
            tX = borda * 3 + larguraRotulo;
        } else {
            tX = borda;
        }
        int tY = (dimensao.getAltura() / 2 + fm.getHeight() / 4);
        g.drawString(texto, dimensao.getPosX() + tX, dimensao.getPosY() + tY);
    }

    private void drawLabel(Graphics g) {
        if (janelaRotulo == null) {
            larguraRotulo = g.getFontMetrics().stringWidth(rotulo);
            Dimensao d = new Dimensao(dimensao.getAltura(), larguraRotulo + borda * 2, dimensao.getPosX(), dimensao.getPosY());
            janelaRotulo = new JanelaBase(d);
            janelaRotulo.setBorda(borda);
            janelaRotulo.setTransparente(transparente);
            janelaRotulo.setCorFundo(corFundo);
        }
        janelaRotulo.desenhaJanela(g);
        janelaRotulo.escreveTexto(rotulo, AlinhamentoHorizontal.ESQ, g, false);
    }

    public String getTexto() {
        return input.getText();
    }

    public void setTexto(String t) {
        input.setText(t);
    }

    public String getRotulo() {
        return rotulo;
    }

    public void setRotulo(String rotulo) {
        this.rotulo = rotulo;
    }

    public void setLarguraTexto(int larguraTexto) {
        this.larguraTexto = larguraTexto;
    }

    public void dispose() {
        this.visivel = false;
        this.setAtivo(false);
        Config.getInstance().getGame().getDisplay().getFrame().requestFocusInWindow();
    }
}
