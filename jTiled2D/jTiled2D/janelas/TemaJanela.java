package jTiled2D.janelas;

import jTiled2D.gfx.ImageLoader;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;


public class TemaJanela {
    
    private BufferedImage[] fundo;
    private BufferedImage[] moldura;
    private BufferedImage[] cursorDIR;
    private BufferedImage[] cursorESQ;

    /**
     * TemaJanela é um objeto que armazena o gráfico das janelas do jogo.
     * O caminho precisa ser de uma imagem em uma grade 8x8, como o exemplo no pkg
     */
    public TemaJanela(String caminho) {
        //carrega o spritesheet
        BufferedImage tema;
        try {
            tema = ImageLoader.carregaImagem(caminho);

        } catch (Exception ex) {
            throw ex;
        }
        init(tema);
    }

    public TemaJanela(InputStream istema){
        try {
            BufferedImage tema = ImageIO.read(istema);
            init(tema);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void init(BufferedImage tema){
        //indice do array
        int index = 0;
        int colW = (tema != null ? tema.getWidth() : 0) / 8;
        int rowH = (tema != null ? tema.getHeight() : 0) / 3;
        //fundo é o primeiro elemento na imagem e deve ser 3x3
        fundo = new BufferedImage[9];
        for(int row=0; row<3; row++){
            for(int col=0; col<3; col++){
                fundo[index] = tema.getSubimage(col*colW, row*rowH, colW, rowH);
                index++;
            }
        }
        //limpa o indice
        index = 0;
        //moldura é o segundo elemento
        moldura = new BufferedImage[9];
        for(int row=0; row<3; row++){
            for(int col=3; col<6; col++){
                moldura[index] = tema.getSubimage(col*colW, row*rowH, colW, rowH);
                index++;
            }
        }
        //limpa o indice
        index = 0;
        //cursor esquerdo
        cursorESQ = new BufferedImage[3];
        for(int row=0; row<3; row++){
            for(int col=6; col<7; col++){
                cursorESQ[index] = tema.getSubimage(col*colW, row*rowH, colW, rowH);
                index++;
            }
        }
        //limpa o indice
        index = 0;
        //cursor direito
        cursorDIR = new BufferedImage[3];
        for(int row=0; row<3; row++){
            for(int col=7; col<8; col++){
                cursorDIR[index] = tema.getSubimage(col*colW, row*rowH, colW, rowH);
                index++;
            }
        }
    }

    public BufferedImage[] getFundo() {
        return fundo;
    }

    public BufferedImage[] getMoldura() {
        return moldura;
    }

    public BufferedImage[] getCursorDIR() {
        return cursorDIR;
    }

    public BufferedImage[] getCursorESQ() {
        return cursorESQ;
    }
    
    
    
    
}
