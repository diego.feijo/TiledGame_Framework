package jTiled2D.janelas;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.util.StringTokenizer;

import jTiled2D.Config;

public class JanelaMessage extends JanelaBase {

    private String[] textoAEscrever, proximo;
    private String texto;
    private boolean esperaComando;
    private boolean contornado;
    private Color corContorno;
    private AlinhamentoVertical alinhamentoVertical;

    /**
     * Uma janela de mensagens
     */
    public JanelaMessage(Dimensao dimensao) {
        super(dimensao);
        esperaComando = false;
    }

    public JanelaMessage(AlinhamentoVertical pos, int altura) {
        super(new Dimensao(altura, Config.getInstance().getLarguraJanela()
                - Config.getInstance().getLarguraTile() * 2,
                Config.getInstance().getLarguraTile(), 0));
        switch (pos) {
            case ACIMA:
                dimensao.setPosY(Config.getInstance().getAlturaTile());
                break;
            case MEIO:
                dimensao.setPosY((Config.getInstance().getAlturaJanela() / 2) - (dimensao.getAltura() / 2));
                break;
            case ABAIXO:
                dimensao.setPosY(Config.getInstance().getAlturaJanela() - dimensao.getAltura() - Config.getInstance().getAlturaTile());
                break;
        }
        this.alinhamentoVertical = pos;
        esperaComando = false;
    }

    public void tick() {
        if (esperaComando && (Config.getInstance().getKeyManager().cancela || isClicado())) {
            if (proximo == null) {
                dispose();
            } else {
                textoAEscrever = proximo;
                esperaComando = false;
            }
        }
    }

    public void render(Graphics g) {
        if (visivel) {
            desenhaJanela(g);
            if (texto != null)
                escreveTexto(g);
        }
    }

    private void escreveTexto(Graphics graphic) {
        textoAEscrever = quebraLinhas(texto, graphic);
        escreveLinhas(graphic);
    }

    private String[] quebraLinhas(String texto, Graphics g) {
        StringTokenizer tok = new StringTokenizer(texto, " ");
        StringBuilder saida = new StringBuilder(texto.length());
        FontMetrics fm = g.getFontMetrics();

        int alturaMaxLinha = dimensao.getLargura() - borda * 2;
        int alturaLinha = 0;
        while (tok.hasMoreTokens()) {
            String palavra = tok.nextToken();
            if (alturaLinha + fm.stringWidth(palavra) > alturaMaxLinha) {
                saida.append("\n");
                alturaLinha = 0;
            }
            saida.append(palavra).append(" ");
            alturaLinha += fm.stringWidth(palavra + " ");
        }
        return saida.toString().split("\n");
    }

    //TODO: corrigir bug para mensagens grandes
    private void escreveLinhas(Graphics g) {
        g.setFont(fontePadrao);
        g.setColor(corPadrao);
        FontMetrics fm = g.getFontMetrics();
        int tY = dimensao.getPosY() + borda;
        int x = dimensao.getPosX();
        if (this.janelaTitulo != null) {
            tY = dimensao.getPosY() + borda + janelaTitulo.dimensao.getAltura();
        }
        int maxLinhas = dimensao.getAltura() / fm.getHeight();
        int linhasAEscrever = textoAEscrever.length > maxLinhas ? maxLinhas : textoAEscrever.length;
        for (int i = 0; i < linhasAEscrever; i++) {
            if (contornado) {
                escreveTextoContornado(g, textoAEscrever[i], x + borda, tY + (i + 1) * fm.getHeight(), corContorno, corPadrao);
            } else {
                g.drawString(textoAEscrever[i], x + borda, tY + (i + 1) * fm.getHeight());
            }
        }
        if (textoAEscrever.length > maxLinhas) {
            proximo = new String[textoAEscrever.length - maxLinhas];
            System.arraycopy(textoAEscrever, maxLinhas, proximo, 0, textoAEscrever.length - maxLinhas);
            //Desenha um icone que indica continuidade do texto
            if (temaJanela != null)
                g.drawImage(temaJanela.getCursorESQ()[0], dimensao.getLargura() - 4, dimensao.getPosY() + dimensao.getAltura() - 8, null);
        }

        esperaComando = true;
    }


    public void setTexto(String texto) {
        this.texto = texto;
    }

    private void dispose() {
        this.visivel = false;
        this.texto = null;
        this.textoAEscrever = null;
    }
}
