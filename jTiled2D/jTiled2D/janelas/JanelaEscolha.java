/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jTiled2D.janelas;

import java.awt.FontMetrics;
import java.awt.Graphics;

import jTiled2D.Config;

public class JanelaEscolha extends JanelaBase {

    private String question;
    private JanelaBotaoMenu choices;
    private AlinhamentoVertical alinhamentoVertical;

    public JanelaEscolha(AlinhamentoVertical alinhamentoVertical, String pergunta, String[] escolhas) {
        super(new Dimensao(0,0,0,0));
        this.alinhamentoVertical = alinhamentoVertical;
        this.question = pergunta;
        this.choices = new JanelaBotaoMenu(dimensao, escolhas);
        switch (alinhamentoVertical) {
            case ACIMA:
                dimensao.setPosY(Config.getInstance().getAlturaTile());
                break;
            case MEIO:
                dimensao.setPosY((Config.getInstance().getAlturaJanela() / 2) - (dimensao.getAltura() / 2));
                break;
            case ABAIXO:
                dimensao.setPosY(Config.getInstance().getAlturaJanela() - dimensao.getAltura() - Config.getInstance().getAlturaTile());
                break;
        }
    }

    public JanelaEscolha() {
        super(new Dimensao(0,0,0,0));
    }    

    public void render(Graphics g) {
        
        if (visivel) {
            FontMetrics fm = g.getFontMetrics();
            dimensao.setLargura(Config.getInstance().getLarguraJanela() - Config.getInstance().getLarguraTile() * 2);
            dimensao.setAltura(fm.getHeight() + borda * 2);
            dimensao.setPosX((Config.getInstance().getLarguraJanela() / 2) - (dimensao.getLargura() / 2));
            if (choices != null) {
                choices.dimensao.setPosX(dimensao.getPosX());
                choices.dimensao.setLargura(dimensao.getLargura());
                switch (alinhamentoVertical) {
                    case ACIMA:
                        dimensao.setPosY(Config.getInstance().getAlturaTile());
                        choices.dimensao.setPosY(dimensao.getPosY() +  dimensao.getAltura());
                        break;
                    case MEIO:
                        dimensao.setPosY((Config.getInstance().getAlturaJanela() / 2) - ((dimensao.getAltura() + choices.dimensao.getAltura()) / 2));
                        choices.dimensao.setPosY(dimensao.getPosY() + dimensao.getAltura());
                        break;
                    case ABAIXO:
                        dimensao.setPosY(Config.getInstance().getAlturaJanela() - dimensao.getAltura() - Config.getInstance().getAlturaTile());
                        choices.dimensao.setPosY(dimensao.getPosY() - choices.dimensao.getAltura());
                        break;
                }
                choices.render(g);
            }
            super.desenhaJanela(g);
            escreveTexto(question, AlinhamentoHorizontal.CENTRO, g, false);
        }
    }

   public int getIndex() {
        if (choices != null && choices.visivel) {
            return choices.getIndice();
        }
        return -1;
    }
}
