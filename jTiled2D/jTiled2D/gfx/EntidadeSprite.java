package jTiled2D.gfx;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

/**
 * Gráficos de entidades que representam atores no jogo.
 * Por padrao sao lidas imagens em grades 4x2 (largura x altura), cada celula
 * com uma grade 3x4 de uma entidade.
 *
 * @author diego
 */
public class EntidadeSprite {

    //Constantes
    private final int NUMCOL = 3;
    private final int NUMLIN = 4;

    private int veloAnim;
    private Animacao movBaixo, movCima,movDir, movEsq;

    /**
     * Novo sprite de Entidade
     *
     * @param caminho  caminho da imagem de sprite
     * @param veloAnim velocidade da animação
     */
    public EntidadeSprite(String caminho, int veloAnim, FolhaSprite.PosSprite posSprite) {
        this.veloAnim = veloAnim; //TODO: tornar enum
        BufferedImage spriteSheet = ImageLoader.carregaImagem(caminho);
        lerImagem(spriteSheet, posSprite);
    }

    public EntidadeSprite(InputStream issprite, int veloAnim, FolhaSprite.PosSprite posSprite){
        this.veloAnim = veloAnim;
        try {
            BufferedImage spriteSheet = ImageIO.read(issprite);
            lerImagem(spriteSheet, posSprite);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Lê a imagem de sprite
     */
    private void lerImagem(BufferedImage spriteSheet, FolhaSprite.PosSprite posSprite) {
        //Carrega a imagem
        spriteSheet = new FolhaSprite(spriteSheet, posSprite).getFolha();

        movBaixo = montarAnimacao(spriteSheet, 0);
        movEsq = montarAnimacao(spriteSheet, 1);
        movDir = montarAnimacao(spriteSheet, 2);
        movCima = montarAnimacao(spriteSheet, 3);
    }

    private Animacao montarAnimacao(BufferedImage spriteSheet, int linha){
        int larguraFrame = spriteSheet.getWidth() / NUMCOL;
        int alturaFrame = spriteSheet.getHeight() / NUMLIN;
        BufferedImage[] bi = new BufferedImage[NUMCOL];
        for(int x = 0; x < NUMCOL; x ++){
            bi[x] = spriteSheet.getSubimage(x*larguraFrame,linha * alturaFrame, larguraFrame, alturaFrame);
        }
        return new Animacao(this.veloAnim, bi);
    }

    public Animacao getMovBaixo() {
        return movBaixo;
    }

    public Animacao getMovCima() {
        return movCima;
    }

    public Animacao getMovDir() {
        return movDir;
    }

    public Animacao getMovEsq() {
        return movEsq;
    }

    public Animacao[] getAnimacoes(){
        return new Animacao[]{movBaixo,movEsq,movDir,movCima};
    }
}
