package jTiled2D.gfx;

import jTiled2D.Config;
import jTiled2D.entidades.Entidade;

public class GameCamera {

    private float limiteX, limiteY;
    private int larguraJanela, alturaJanela;
    private int larguraMapa, alturaMapa;
    private int larguraTile, alturaTile;

    /**
     * Controla o que deve ser apresentado na tela
     *
     * @param limiteX limite x
     * @param limiteY limite y
     */
    public GameCamera(float limiteX, float limiteY) {
        this.limiteX = limiteX;
        this.limiteY = limiteY;
        init();
    }

    private void init(){
        this.larguraJanela = Config.getInstance().getLarguraJanela();
        this.alturaJanela = Config.getInstance().getAlturaJanela();
    }

    /**
     * Ajusta as dimensões do mapa a fins de cálculo
     */
    protected void setDimensaoMapa() {
        this.larguraMapa = Config.getInstance().getMapaAtual().getWidth();
        this.alturaMapa = Config.getInstance().getMapaAtual().getHeight();
        this.larguraTile = Config.getInstance().getLarguraTile();
        this.alturaTile = Config.getInstance().getAlturaTile();
    }

    /**
     * Previne que não sejam apresentados espaços vazios
     */
    public void verificaVazio() {
        setDimensaoMapa();
        if (limiteX < 0) {
            limiteX = 0;
        } else if (limiteX > larguraMapa * larguraTile - larguraJanela) {
            limiteX = larguraMapa * larguraTile - larguraJanela;
        }
        if (limiteY < 0) {
            limiteY = 0;
        } else if (limiteY > alturaMapa * alturaTile - alturaJanela) {
            limiteY = alturaMapa * alturaTile - alturaJanela;
        }

    }

    /**
     * Centraliza a camera em uma Entidade
     *
     * @param e a entidade
     */
    public void centralizaEm(Entidade e) {
        limiteX = e.x - larguraJanela / 2 + e.getLargura() / 2;
        limiteY = e.y - alturaJanela / 2 + e.getAltura() / 2;
        verificaVazio();
    }

    /**
     * Move a camera
     *
     * @param xAmt posição X
     * @param yAmt posição Y
     */
    public void mover(float xAmt, float yAmt) {
        limiteX += xAmt;
        limiteY += yAmt;
        verificaVazio();
    }


    public float getLimiteX() {
        return limiteX;
    }

    public void setLimiteX(float limiteX) {
        this.limiteX = limiteX;
    }

    public float getLimiteY() {
        return limiteY;
    }

    public void setLimiteY(float limiteY) {
        this.limiteY = limiteY;
    }
}
