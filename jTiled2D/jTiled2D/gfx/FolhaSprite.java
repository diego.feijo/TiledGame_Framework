package jTiled2D.gfx;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 * Esta classe lê uma imagem e a subdivide em vários sprites, com o intuito de utilizar uma
 * folha de sprite única para manter todas as variadas posições de uma entidade.
 */
public class FolhaSprite {

    public enum PosSprite{
        POS1(0), POS2 (1), POS3 (2), POS4 (3), POS5 (4), POS6 (5), POS7 (6), POS8 (7);
        private final int pos;

        PosSprite(int pos){ this.pos = pos;}

        int pos(){return this.pos;}
    }

    private final int NUMCOL = 4;
    private final int NUMLIN = 2;

    private BufferedImage folha;


    /**Cria uma nova FolhaSprite
     * @param folha a BufferedImage que representa a folha de sprites
     * */
    public FolhaSprite(BufferedImage folha) {
        this.folha = folha;
    }

    /** Cria uma nova FolhaSprite
     * @param folha um BufferedImage que representa a folha de sprites
     * @param posSprite a posicao da entidade em uma folha 4x2 (largura x altura).
     *                  Sendo 1 a posicao mais acima a esquerda e 8 a posicao mais abaixo a direita.
     * */
    public FolhaSprite(BufferedImage folha, PosSprite posSprite){
        //Calcula a imagem baseado na posicao do sprite
        int largura = folha.getWidth() / NUMCOL;
        int altura = folha.getHeight() / NUMLIN;
        int x = (posSprite.pos % 4) * largura;
        int y = posSprite.pos <= 3 ? 0 : altura;
        //Corta a imagem
        this.folha = folha.getSubimage(x,y,largura,altura);
    }

    /**
     * Corta a folha em uma subimagem
     *
     * @return um BufferedImage que representa a subimagem cortada
     */
    public BufferedImage cortar(int x, int y, int largura, int altura) {
        return folha.getSubimage(x, y, largura, altura);
    }

    /**
     * Define qual cor dessa FolhaSprite deve ser transparente
     *
     * @param color a cor a ser transparente
     */
    public void setCorTransparente(Color color) {
        //Aplica um canal alpha no BufferedImage
        int largura = folha.getWidth();
        int altura = folha.getHeight();
        int tipo = BufferedImage.TYPE_INT_ARGB;
        BufferedImage bia = new BufferedImage(largura, altura, tipo);
        Graphics2D g = bia.createGraphics();
        g.drawImage(folha, 0, 0, null);
        g.dispose();
        folha = bia;

        //fixa a cor transparente
        for (int y = 0; y < folha.getHeight(); ++y) {
            for (int x = 0; x < folha.getWidth(); ++x) {
                int argb = folha.getRGB(x, y);
                if (argb == color.getRGB()) {
                    folha.setRGB(x, y, 0);
                }
            }
        }
    }

    public BufferedImage getFolha() {
        return folha;
    }
}
