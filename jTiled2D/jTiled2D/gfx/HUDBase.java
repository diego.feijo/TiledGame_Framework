package jTiled2D.gfx;

import java.awt.Graphics;

import jTiled2D.janelas.Dimensao;
import jTiled2D.janelas.JanelaMessage;

/**
 * HUD (Heads Up Display)  é uma forma de apresentar informações importantes ao jogador
 * na tela. Esta classe representa uma base para um HUD simples.
 */
public abstract class HUDBase {

    private String mensagem;

    private JanelaMessage caixaMensagem;

    protected int hudx, hudy;

    public abstract void tick();

    public abstract void render(Graphics g);
    
    public abstract void atribuirJanelaMensagem(Dimensao dimensao);

    public void atribuirVisibilidadeCaixaMensagem(boolean v) {
        caixaMensagem.setVisivel(v);
    }

    public void atribuirMensagem(String msg) {
        this.mensagem = msg;

    }
}
