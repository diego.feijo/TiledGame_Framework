
package jTiled2D.gfx;

import java.awt.image.BufferedImage;

public class Animacao {
    private int velocidade, index;
    private long ultimoTempo, timer;
    private BufferedImage[] frames;

    /**
     * Controla as animações do jogo
     *
     * @param velocidade a velocidade da animação
     * @param frames     as frames da animação
     */
    public Animacao(int velocidade, BufferedImage[] frames) {
        this.velocidade = velocidade;
        this.frames = frames;
        index = 0;
        ultimoTempo = System.currentTimeMillis();
        timer = 0;
    }

    /**
     * Ciclo da animação
     */
    public void tick() {
        timer += System.currentTimeMillis() - ultimoTempo;
        ultimoTempo = System.currentTimeMillis();

        if (timer > velocidade) {
            index++;
            timer = 0;
            if (index >= frames.length) {
                index = 0;
            }
        }

    }

    /**
     * Permite exibir o frame atual da animação
     *
     * @return um BufferedImage que representa o frame atual
     */
    public BufferedImage getFrameAtual() {
        return frames[index];
    }
}
