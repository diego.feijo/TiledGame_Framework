package jTiled2D.gfx;

import java.awt.image.BufferedImage;

/**
 * Permite controlar os recursos visuais dos atores do jogo.
 * Implemente esta classe para montar com facilidade os sprites.
 */
public abstract class Assets {

    private FolhaSprite ss;
    protected int largura, altura;
    private BufferedImage[] sprites;

    /**
     * Novo Asset pelo caminho indicado em caminho
     *
     * @param caminho o caminho da imagem. Deve ser um caminho válido ou retornará IllegalArgumentException.
     */
    public Assets(String caminho) {
        this.ss = new FolhaSprite(ImageLoader.carregaImagem(caminho));
    }

    /**
     * Novo Asset pelo caminho indicado em caminho. A imagem encontrada será subdividida horizontalmente pelo numDeImagens
     *
     * @param path         o caminho da imagem. Deve ser um caminho válido ou retornará IllegalArgumentException.
     * @param largura      largura do sprite, para subdividir a imagem.
     * @param altura       altura do sprite, para subdividir a imagem.
     * @param numDeImagens o número de sprites esperado na imagem.
     */
    public Assets(String path, int largura, int altura, int numDeImagens) {
        this.ss = new FolhaSprite(ImageLoader.carregaImagem(path));
        this.largura = largura;
        this.altura = altura;
        sprites = new BufferedImage[numDeImagens];
        for (int i = 0; i < sprites.length; i++) {
            sprites[i] = ss.cortar(i*largura, altura, largura, altura);
        }
    }

    /**
     * Inicia os sprites. Implemente este método com o código que deve ser rodado ao iniciar o Asset
     */
    public abstract void init();


    public BufferedImage getSprite(int frame) {
        return sprites[frame];
    }

    public BufferedImage[] getSprites(){
        return sprites;
    }
}
