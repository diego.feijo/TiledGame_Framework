/*
Graphic controller to load the game images.
*/
package jTiled2D.gfx;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * Utilitário para carregar imagens.
 */
public class ImageLoader {

    /**
     * Carrega uma imagem para memória, se o caminho fornecido for válido.
     *
     * @param caminho o caminho de uma imagem
     * @throws IllegalArgumentException se o caminho for inválido
     */
    public static BufferedImage carregaImagem(String caminho){
        try {
            File file = new File(caminho);
            //Confere se o caminho é um diretório
            if (file.isFile()) {
                BufferedImage img =ImageIO.read(file);
                if(img == null) throw new FileNotFoundException(String.format("Arquivo: %s não pode ser lido.", caminho));
                return img;
            }
            else
                throw new IllegalArgumentException(String.format("%s não é um arquivo válido.", caminho));
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(1);
        }
        return null;
    }

}
