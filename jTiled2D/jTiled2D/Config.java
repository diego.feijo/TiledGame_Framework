package jTiled2D;

import jTiled2D.gfx.GameCamera;
import jTiled2D.input.KeyManager;
import jTiled2D.input.MouseManager;
import jTiled2D.janelas.TemaJanela;
import jTiled2D.mapas.Mapa;

import java.awt.*;
import java.io.InputStream;

/**
 * Esta classe determina as configurações gerais do jogo.
 * Suas propriedades são alteradas de acordo com o estado do jogo, por isso seu acesso é estático
 */
public class Config {

    //Tratamentos para singleton
    public static Config instance;

    private Config() {
    }

    public static Config getInstance(){
        if(instance == null)
            instance = new Config();

        return instance;
    }
    //Fim dos tratamentos para singleton

    private Game game;
    private int larguraTile;
    private int alturaTile;
    private int larguraJanela;
    private int alturaJanela;
    private Mapa mapaAtual;
    private KeyManager keyManager;
    private MouseManager mouseManager;
    private GameCamera gameCamera;
    private TemaJanela temaJanela;
    private Font fontePadrao;
    private Color corPadrao;
    private String nomeJogo;
    private int fps;
    private InputStream tilesetAtual;

    public int getAlturaTile() {
        return alturaTile;
    }

    public void setAlturaTile(int altura) {
        alturaTile = altura;
    }

    public int getLarguraTile() {
        return larguraTile;
    }

    public void setLarguraTile(int largura) {
        larguraTile = largura;
    }

    public Mapa getMapaAtual() {
        return mapaAtual;
    }

    public void setMapaAtual(Mapa mapaAtual) {
        this.mapaAtual = mapaAtual;
    }

    public KeyManager getKeyManager() {
        if(keyManager == null)
            keyManager = new KeyManager();
        return keyManager;
    }

    public void setKeyManager(KeyManager keyManager) {
        this.keyManager = keyManager;
    }

    public GameCamera getGameCamera() {
        return gameCamera;
    }

    public void setGameCamera(GameCamera gameCamera) {
        this.gameCamera = gameCamera;
    }

    public int getLarguraJanela() {
        return larguraJanela;
    }

    public void setLarguraJanela(int larguraJanela) {
        this.larguraJanela = larguraJanela;
    }

    public int getAlturaJanela() {
        return alturaJanela;
    }

    public void setAlturaJanela(int alturaJanela) {
        this.alturaJanela = alturaJanela;
    }

    public void setMouseManager(MouseManager mouseManager) {
        this.mouseManager = mouseManager;
    }

    public MouseManager getMouseManager() {
        return mouseManager;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public TemaJanela getTemaJanela() {
        return temaJanela;
    }

    public void setTemaJanela(TemaJanela temaJanela) {
        this.temaJanela = temaJanela;
    }

    public Game getGame() {
        return game;
    }

    public Font getFontePadrao() {
        return fontePadrao;
    }

    public void setFontePadrao(Font fontePadrao) {
        this.fontePadrao = fontePadrao;
    }

    public Color getCorPadrao() {
        return corPadrao;
    }

    public void setCorPadrao(Color corPadrao) {
        this.corPadrao = corPadrao;
    }

    public String getNomeJogo() {
        return nomeJogo;
    }

    public void setNomeJogo(String nomeJogo) {
        this.nomeJogo = nomeJogo;
    }

    public int getFps() {
        return fps;
    }

    public void setFps(int fps) {
        this.fps = fps;
    }

    public InputStream getTilesetAtual() {
        return tilesetAtual;
    }

    public void setTilesetAtual(InputStream tilesetAtual) {
        this.tilesetAtual = tilesetAtual;
    }
}
