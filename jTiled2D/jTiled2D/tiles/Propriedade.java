package jTiled2D.tiles;

import java.util.HashMap;

public class Propriedade {
    private int idTile;
    private HashMap<String, String> props = new HashMap<>();

    public Propriedade(int idTile) {
        this.idTile = idTile;
    }

    public int getIdTile() {
        return idTile;
    }

    public void put(String name, String value){
        props.put(name, value);
    }

    public String get(String name){
        return props.get(name);
    }


}