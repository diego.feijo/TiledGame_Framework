package jTiled2D.tiles;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import jTiled2D.Config;
import jTiled2D.gfx.ImageLoader;
import jTiled2D.gfx.FolhaSprite;

import javax.imageio.ImageIO;

/**
 *
 * @author diego
 */
public class Tileset {

    protected String name, primeiroGID;
    protected int numTiles;
    protected HashMap<Integer, BufferedImage> tiles = new HashMap<>();
    protected List<Propriedade> propriedades = new ArrayList<>();

    //Imagem
    protected String transparenciaImg;
    protected int larguraImg, alturaImg;
    protected int larguraTile, alturaTile;

    public Tileset() {

    }

    public Tileset(String name, String primeiroGID, int numTiles, InputStream inputstreamImagem,
                   String transparenciaImg, int larguraImg, int alturaImg, int larguraTile, int alturaTile) {
        this.name = name;
        this.primeiroGID = primeiroGID;
        this.numTiles = numTiles;
        this.transparenciaImg = transparenciaImg;
        this.larguraImg = larguraImg;
        this.alturaImg = alturaImg;
        this.larguraTile = larguraTile;
        this.alturaTile = alturaTile;

        try {
            FolhaSprite sprite = new FolhaSprite(ImageIO.read(inputstreamImagem));
            initSpriteSheet(sprite);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Tileset(String name, String primeiroGID, int numTiles, String caminhoImg,
                   String transparenciaImg, int larguraImg, int alturaImg, int larguraTile, int alturaTile) {
        this.name = name;
        this.primeiroGID = primeiroGID;
        this.numTiles = numTiles;
        this.transparenciaImg = transparenciaImg;
        this.larguraImg = larguraImg;
        this.alturaImg = alturaImg;
        this.larguraTile = larguraTile;
        this.alturaTile = alturaTile;
        FolhaSprite sprite = new FolhaSprite(ImageLoader.carregaImagem(caminhoImg));
        initSpriteSheet(sprite);
    }

    protected void initSpriteSheet(FolhaSprite sprite) {
        try {
            int col = larguraImg / larguraTile;
            int lin = alturaImg / alturaTile;

            if(transparenciaImg != null && transparenciaImg.trim() !="") {
                Color tcor = new Color(
                        Integer.valueOf(transparenciaImg.substring(0, 2), 16),
                        Integer.valueOf(transparenciaImg.substring(2, 4), 16),
                        Integer.valueOf(transparenciaImg.substring(4, 6), 16));
                sprite.setCorTransparente(tcor);
            }
            int gid = 1;
            for (int r = 0; r < lin; r++) {
                for (int c = 0; c < col; c++) {
                    BufferedImage tile = sprite.cortar(c * larguraTile, r * alturaTile, larguraTile, alturaTile);
                    tiles.put(gid, tile);
                    gid++;
                }
            }
            //Como tudo deu certo, informa a config os tamanhos dos tiles
            Config cfg = Config.getInstance();
            cfg.setAlturaTile(this.alturaTile);
            cfg.setLarguraTile(this.larguraTile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }

    public String getPrimeiroGID() {
        return primeiroGID;
    }

    public int getNumTiles() {
        return numTiles;
    }

    public String getTransparenciaImg() {
        return transparenciaImg;
    }

    public int getImgWidth() {
        return larguraImg;
    }

    public int getImgHeight() {
        return alturaImg;
    }

    public BufferedImage getTileTexture(int id) {
        return tiles.get(id);
    }

    public List<Propriedade> getPropriedades() {
        return propriedades;
    }
    
    public void addProperty(Propriedade p){
        propriedades.add(p);
    }
    
    public Propriedade getPropertyByTileID(int id){
        if(this.propriedades != null){
            for(Propriedade p: propriedades){
                if(p.getIdTile() == id)
                    return p;
            }            
        }
        return null;
    }
}
