package jTiled2D.tiles;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class Tile {

    private int x, y, largura, altura, id;
    private boolean solido;

    private BufferedImage textura;

    public Tile(BufferedImage textura, int x, int y, int largura, int altura) {
        this.textura = textura;
        this.x = x;
        this.y = y;
        this.largura = largura;
        this.altura = altura;
    }

    public void tick() {

    }

    public void render(Graphics g) {
        g.drawImage(textura, x* largura, y* altura, largura, altura, null);
    }
    
    public void render(Graphics g, int x, int y){
        g.drawImage(textura, x, y, largura, altura, null);
    }

    public boolean isSolido() {
        return solido;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getLargura() {
        return largura;
    }

    public int getAltura() {
        return altura;
    }

    public void setSolido(boolean solido) {
        this.solido = solido;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public boolean hasTexture(){
        return textura != null;
    }
    

}
