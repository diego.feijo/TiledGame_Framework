
package jTiled2D;

import jTiled2D.estados.Estado;
import jTiled2D.input.KeyManager;
import jTiled2D.input.MouseManager;
import jTiled2D.janelas.TemaJanela;

import java.io.InputStream;

/**
 *
 * @author diego
 */
public class Launcher {
    
    public static void launchGame(String nomeJogo, int larguraJanela, int alturaJanela, int fps,
                                  String caminhoTemaJanela,Estado estadoInicial){
        try {
            Config.getInstance().setNomeJogo(nomeJogo);
            Config.getInstance().setAlturaJanela(alturaJanela);
            Config.getInstance().setLarguraJanela(larguraJanela);
            Config.getInstance().setFps(fps);
            Config.getInstance().setTemaJanela(new TemaJanela(caminhoTemaJanela));
            Config.getInstance().setKeyManager(new KeyManager());
            Config.getInstance().setMouseManager(new MouseManager());
            //Game controller
            Game game = new Game(estadoInicial);
            Config.getInstance().setGame(game);
            game.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void launchGame(String nomeJogo, int larguraJanela, int alturaJanela, int fps,
                                  InputStream temaJanela, Estado estadoInicial){
        try {
            Config.getInstance().setNomeJogo(nomeJogo);
            Config.getInstance().setAlturaJanela(alturaJanela);
            Config.getInstance().setLarguraJanela(larguraJanela);
            Config.getInstance().setFps(fps);
            Config.getInstance().setTemaJanela(new TemaJanela(temaJanela));
            Config.getInstance().setKeyManager(new KeyManager());
            Config.getInstance().setMouseManager(new MouseManager());
            //Game controller
            Game game = new Game(estadoInicial);
            Config.getInstance().setGame(game);
            game.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
