/*
In this class we prepare the display where our game will be show in
*/
package jTiled2D;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;


public class Display {
    private JFrame frame;
    private Canvas canvas;
    
    private String titulo;
    private int largura, altura;
    
    /**
     * Onde o jogo sera renderizado
     * 
     * @param titulo titulo da janela
     * @param l largura
     * @param a altura
     */
    public Display(String titulo, int l, int a){
        this.titulo = titulo;
        this.largura = l;
        this.altura = a;
        
        init();
    }
    
    /**
     * Cria o display
     */
    private void init(){
        frame = new JFrame(titulo);
        frame.setSize(largura, altura);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        
        canvas = new Canvas();
        canvas.setPreferredSize(new Dimension(largura, altura));
        canvas.setMaximumSize(new Dimension(largura, altura));
        canvas.setMinimumSize(new Dimension(largura, altura));
        canvas.setBackground(Color.BLACK);
        canvas.setFocusable(false);
        
        frame.add(canvas);
        frame.pack();
               
    }

    public Canvas getCanvas() {
        return canvas;
    }
    
    public JFrame getFrame(){
        return frame;
    }
}
