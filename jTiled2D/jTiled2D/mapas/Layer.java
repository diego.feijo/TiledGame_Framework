package jTiled2D.mapas;

import jTiled2D.tiles.Tile;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe que representa as camadas de Tiles de um mapa
 */
public class Layer {

    private String nomeLayer;
    private int largura, altura;
    private List<Tile> tiles = new ArrayList<>();

    public Layer(String nomeLayer, int layerWidth, int altura) {
        this.nomeLayer = nomeLayer;
        this.largura = layerWidth;
        this.altura = altura;
    }

    public Tile getTile(int x, int y) {
        for (Tile t : tiles) {
            if (t.getX() == x && t.getY() == y) {
                return t;
            }
        }
        return null;
    }

    public List<Tile> getTiles() {
        return tiles;
    }

    public void putTile(Tile t) {
        this.tiles.add(t);
    }

    public String getNomeLayer() {
        return nomeLayer;
    }

    public int getLargura() {
        return largura;
    }

    public int getAltura() {
        return altura;
    }

}
