package jTiled2D.mapas;

import java.util.HashMap;


public class MapaObjeto {

    private int id, largura, altura;
    private float posX, posY;
    private String nome, grupo, tipo;
    private HashMap<String, String> props = new HashMap<>();

    public MapaObjeto(int id, float posX, float posY, int largura, int altura, String nome, String grupo, String tipo) {
        this.id = id;
        this.posX = posX;
        this.posY = posY;
        this.largura = largura;
        this.altura = altura;
        this.nome = nome;
        this.grupo = grupo;
        this.tipo = tipo;
    }

    public void adicionaPropriedade(String nome, String valor) {
        props.put(nome, valor);
    }

    public String getPropriedade(String nome) {
        return props.get(nome);
    }

    public int getId() {
        return id;
    }

    public float getPosX() {
        return posX;
    }

    public float getPosY() {
        return posY;
    }

    public int getLargura() {
        return largura;
    }

    public int getAltura() {
        return altura;
    }

    public String getNome() {
        return nome;
    }

    public String getGrupo() {
        return grupo;
    }

    public String getTipo() {
        return tipo;
    }

}
