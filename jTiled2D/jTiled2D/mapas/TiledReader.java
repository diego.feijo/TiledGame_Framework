package jTiled2D.mapas;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import jTiled2D.Config;
import jTiled2D.entidades.EntidadeGerenciador;
import jTiled2D.tiles.Propriedade;
import jTiled2D.tiles.Tile;
import jTiled2D.tiles.Tileset;

public class TiledReader {

    //Nao deve ser instanciado
    private TiledReader() {
    }

    public static Mapa lerArquivo(InputStream ismap) {
        try {
            //Tratamentos do xml
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(ismap);
            doc.getDocumentElement().normalize();

            return lerArquivoInternal(doc);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Mapa lerArquivo(String caminho) {
        try {
            //Tratamentos do xml
            File xmlMap = new File(caminho);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlMap);

            doc.getDocumentElement().normalize();

            return lerArquivoInternal(doc);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static Mapa lerArquivoInternal(Document doc) {
        try {
            //Propriedades do mapa
            MapaConfig mapconfig = getConfig(doc);
            //Propriedades do tileset
            Tileset tileset = getTileset(doc, mapconfig);
            //Popula os layers do mapconfig
            populaLayers(doc, mapconfig, tileset);
            //Popula os objetos do mapconfig
            populaObjetos(doc, mapconfig);

            //Cria o EntidadeGerenciador
            EntidadeGerenciador em = new EntidadeGerenciador(mapconfig.getEntidades());
            //Retorna o mapa
            Mapa mapa = new Mapa(mapconfig, em);
            Config.getInstance().setMapaAtual(mapa);
            return mapa;
        } catch (ParserConfigurationException | SAXException | IOException | IllegalArgumentException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /*
     * Busca os dados de configuracao do XML
     */
    private static MapaConfig getConfig(Document doc) {
        //O objeto a ser retornado
        MapaConfig mapconfig = new MapaConfig();

        NodeList mapProp = doc.getElementsByTagName("map");
        Node mNode = mapProp.item(0);
        if (mNode != null && mNode.getNodeType() == Node.ELEMENT_NODE) {
            Element mElement = (Element) mNode;
            mapconfig.setVersao(mElement.getAttribute("version"));
            mapconfig.setOrientacao(mElement.getAttribute("orientation"));
            mapconfig.setOrdemRenderizacao(mElement.getAttribute("renderorder"));
            mapconfig.setLargura(Integer.parseInt(mElement.getAttribute("width")));
            mapconfig.setAltura(Integer.parseInt(mElement.getAttribute("height")));
            mapconfig.setLarguraTile(Integer.parseInt(mElement.getAttribute("tilewidth")));
            mapconfig.setAlturaTile(Integer.parseInt(mElement.getAttribute("tileheight")));
            mapconfig.setIdProxObjeto(Integer.parseInt(mElement.getAttribute("nextobjectid")));
        }

        return mapconfig;
    }

    /*
    Monta o tileset
     */
    private static Tileset getTileset(Document doc, MapaConfig mapconfig) {
        // Tileset a retornar
        Tileset tileset = new Tileset();
        NodeList tsProp = doc.getElementsByTagName("tileset");
        Node tsNode = tsProp.item(0);
        if (tsNode != null && tsNode.getNodeType() == Node.ELEMENT_NODE) {
            Element tsElement = (Element) tsNode;
            String firstgid = tsElement.getAttribute("firstgid");
            String name = tsElement.getAttribute("name");
            int tilecount = Integer.parseInt(tsElement.getAttribute("tilecount"));

            //Propriedades de imagem
            Node imageNode = tsElement.getElementsByTagName("image").item(0);
            if (imageNode != null && imageNode.getNodeType() == Node.ELEMENT_NODE) {
                Element image = (Element) imageNode;
                int imgWidth = Integer.parseInt(image.getAttribute("width"));
                int imgHeight = Integer.parseInt(image.getAttribute("height"));
                String trans = image.getAttribute("trans");
                //Confere se a config tem o tileset atual
                InputStream inputStreamTileSet = Config.getInstance().getTilesetAtual();
                String source = null;
                if(inputStreamTileSet == null){
                    source = image.getAttribute("source");
                    tileset = new Tileset(name, firstgid, tilecount, source, trans, imgWidth, imgHeight, mapconfig.getLarguraTile(), mapconfig.getAlturaTile());
                } else{
                    tileset = new Tileset(name, firstgid, tilecount, inputStreamTileSet, trans, imgWidth, imgHeight, mapconfig.getLarguraTile(), mapconfig.getAlturaTile());
                }




            }

            //Propriedades de Tiles
            NodeList tilePropertiesNL = tsElement.getElementsByTagName("tile");
            for (int i = 0; i < tilePropertiesNL.getLength(); i++) {
                Node n = tilePropertiesNL.item(i);
                if (n != null && n.getNodeType() == Node.ELEMENT_NODE) {
                    Element tProperty = (Element) n;
                    int tpid = Integer.parseInt(tProperty.getAttribute("id")) + 1; //no TILED gui = id+1
                    Propriedade p = new Propriedade(tpid);
                    NodeList tilePropValueNL = tProperty.getElementsByTagName("property");
                    for (int j = 0; j < tilePropValueNL.getLength(); j++) {
                        Node pn = tilePropValueNL.item(j);
                        if (pn != null && pn.getNodeType() == Node.ELEMENT_NODE) {
                            Element prop = (Element) pn;
                            String propname = prop.getAttribute("name");
                            String propvalue = prop.getAttribute("value");
                            p.put(propname, propvalue);
                        }
                    }
                    tileset.addProperty(p);
                }
            }
            mapconfig.setTileset(tileset);
        }

        return tileset;
    }

    /*
    Popula os layers em um MapaConfig
     */
    private static void populaLayers(Document doc, MapaConfig mapconfig, Tileset tileset) {
        //Layers
        NodeList layers = doc.getElementsByTagName("layer");
        for (int i = 0; i < layers.getLength(); i++) {
            Node lNode = layers.item(i);
            if (lNode != null && lNode.getNodeType() == Node.ELEMENT_NODE) {
                Element lElement = (Element) lNode;
                String layerName = lElement.getAttribute("name");
                int layerWidth = Integer.parseInt(lElement.getAttribute("width"));
                int layerHeigh = Integer.parseInt(lElement.getAttribute("height"));
                Layer layer = new Layer(layerName, layerWidth, layerHeigh);

                Element dElement = (Element) lElement.getElementsByTagName("data").item(0);
                NodeList tiles = dElement.getElementsByTagName("tile");
                int x = 0;
                int y = 0;
                for (int j = 0; j < tiles.getLength(); j++) {
                    Node tileNode = tiles.item(j);
                    if (tileNode != null && tileNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element tile = (Element) tileNode;
                        int gid = -1;
                        try {
                            gid = Integer.parseInt(tile.getAttribute("gid"));
                            //cria os tiles
                            Tile newtile = new Tile(tileset.getTileTexture(gid), x, y, mapconfig.getLarguraTile(), mapconfig.getAlturaTile());
                            newtile.setId(gid);
                            newtile.setSolido(true);
                            Propriedade p = tileset.getPropertyByTileID(gid);
                            if (p != null) {
                                String solid = p.get("solid");
                                if (solid != null && solid.equalsIgnoreCase("false")) {
                                    newtile.setSolido(false);
                                }
                            }
                            layer.putTile(newtile);
                        } catch (NumberFormatException nfe) {
                            //se nao encontrou um tile, avanca na grade
                        } finally {
                            x++;
                            if (x == layer.getLargura()) {
                                y++;
                                x = 0;
                            }
                        }
                    }
                }
                mapconfig.addLayer(layer);
            }
        }
    }

    /*
    Popula os objetos em um MapaConfig
     */
    private static void populaObjetos(Document doc, MapaConfig mapconfig) {
        NodeList objGroup = doc.getElementsByTagName("objectgroup");
        for (int i = 0; i < objGroup.getLength(); i++) {
            Node oNode = objGroup.item(i);
            if (oNode != null && oNode.getNodeType() == Node.ELEMENT_NODE) {
                Element ogElement = (Element) oNode;
                String objGroupName = ogElement.getAttribute("name");
                NodeList objects = ogElement.getElementsByTagName("object");
                for (int o = 0; o < objects.getLength(); o++) {
                    Node objNode = objects.item(o);
                    if (objNode != null && objNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element obj = (Element) objNode;
                        int id = Integer.parseInt(obj.getAttribute("id"));
                        String objname = obj.getAttribute("name");
                        String objType = obj.getAttribute("type");
                        float posx = Float.parseFloat(obj.getAttribute("x"));
                        float posy = Float.parseFloat(obj.getAttribute("y"));
                        int objwidth = Integer.parseInt(obj.getAttribute("width"));
                        int objheight = Integer.parseInt(obj.getAttribute("height"));
                        //Criando os objetos de mapa
                        MapaObjeto mo = new MapaObjeto(id, posx, posy, objwidth, objheight, objname, objGroupName, objType);
                        //Propriedades
                        Node objProp = obj.getElementsByTagName("properties").item(0);
                        if (objProp != null && objProp.getNodeType() == Node.ELEMENT_NODE) {
                            NodeList objPpts = ((Element) objProp).getElementsByTagName("property");
                            for (int op = 0; op < objPpts.getLength(); op++) {
                                Node objppty = objPpts.item(op);
                                if (objppty.getNodeType() == Node.ELEMENT_NODE) {
                                    Element opElement = (Element) objppty;
                                    String pptyname = opElement.getAttribute("name");
                                    String pptyvalue = opElement.getAttribute("value");
                                    mo.adicionaPropriedade(pptyname, pptyvalue);
                                }
                            }
                        }
                        mapconfig.adicionaObjeto(mo);
                    }
                }
            }
        }
    }
}
