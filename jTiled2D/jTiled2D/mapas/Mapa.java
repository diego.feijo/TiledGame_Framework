package jTiled2D.mapas;

import java.awt.Graphics;
import java.util.List;

import jTiled2D.Config;
import jTiled2D.entidades.EntidadeGerenciador;
import jTiled2D.entidades.Entidade;
import jTiled2D.excecoes.CameraInvalidaException;
import jTiled2D.excecoes.JanelaException;
import jTiled2D.excecoes.TileException;
import jTiled2D.gfx.GameCamera;
import jTiled2D.tiles.Tile;

/**
 * Representa um mapa do jogo
 */
public class Mapa {

    protected MapaConfig config;

    protected int[] xSpawn;
    protected int[] ySpawn;
    protected GameCamera camera;
    protected int alturaTile, larguraTile, alturaJanela, larguraJanela;


    protected EntidadeGerenciador entidadeGerenciador;

    /**
     * Constrói o mapa do jogo
     *
     * @param config um MapaConfig
     * @throws IllegalArgumentException se algum parâmetro for nulo
     */
    public Mapa(MapaConfig config, EntidadeGerenciador em) throws Exception {
        this.config = config;
        this.entidadeGerenciador = em;
        init();
    }

    private void init() throws Exception {
        Config config = Config.getInstance();
        this.camera = config.getGameCamera();
        this.alturaTile = config.getAlturaTile();
        this.larguraTile = config.getLarguraTile();
        this.alturaJanela = config.getAlturaJanela();
        this.larguraJanela = config.getLarguraJanela();

        //validações
        if (config == null) throw new IllegalArgumentException();
        if (camera == null) throw new CameraInvalidaException();
        if (alturaTile <= 0 || larguraTile <= 0) throw new TileException();
        if (alturaJanela <= 0 || larguraJanela <= 0) throw new JanelaException();
    }

    /**
     * Atualização do mapa
     */
    public void tick() {
        entidadeGerenciador.tick();
        config.setEntidades(entidadeGerenciador.getEntidades());
    }

    /**
     * Renderiza o mapa
     */
    public void render(Graphics g) {
        float limiteX = camera.getLimiteX();
        float limiteY = camera.getLimiteY();
        int alturaMapa = config.getAltura();
        int larguraMapa = config.getLargura();

        //Limites da camera
        int xInicio = (int) Math.max(0, limiteX / larguraTile);
        int xFim = (int) Math.min(larguraMapa, (limiteX + larguraJanela) / larguraTile + 1);
        int yInicio = (int) Math.max(0, limiteY / alturaTile);
        int yFim = (int) Math.min(alturaMapa, (limiteY + alturaJanela) / alturaTile + 1);

        for (int l = 0; l < config.getLayers().size(); l++) {
            for (int y = yInicio; y < yFim; y++) {
                for (int x = xInicio; x < xFim; x++) {
                    //Renderiza o Tile
                    Tile t = getTile(x, y, l);
                    if (t != null)
                        t.render(g, (int) (x * larguraTile - limiteX), (int) (y * alturaTile - limiteY));
                }
            }
        }
        //Renderiza as entidades
        entidadeGerenciador.render(g);
    }

    /**
     * Retorna o Tile na posição informada
     *
     * @param x: coordenada X
     * @param y: coordenada Y
     * @param l: o layer em que se encontra o tile
     * @return um Tile ou null se não encontrado
     */
    public Tile getTile(int x, int y, int l) {
        //Se não pode encontrar o tile
        if (x < 0 || y < 0 || x >= config.getLargura() || y >= config.getAltura()) {
            return null;
        }
        //Retorna o Tile no layer indicado
        return config.getLayers().get(l).getTile(x, y);
    }

    /**
     * Retorna o Tile no layer mais alto
     *
     * @param x coordenada X
     * @param y coordenada Y
     * @return um Tile ou null se não encontrado
     */
    public Tile getTile(int x, int y) {
        //Se não pode encontrar o tile
        if (x < 0 || y < 0 || x >= config.getLargura() || y >= config.getAltura()) {
            return null;
        }

        //Retorna o tile do layer mais alto
        List<Layer> layers = config.getLayers();
        for (int i = layers.size() - 1; i >= 0; i--) {
            Tile t = layers.get(i).getTile(x, y);
            if (t != null && t.hasTexture())
                return t;
        }
        return null;

    }

    /**
     * Retorna uma entidade dado suas coordenadas
     *
     * @param x coordenada X
     * @param y coordenada Y
     * @return null se não encontrado ou um Entidade
     */
    public Entidade getEntity(int x, int y) {
        int ex, ey;
        if (x < 0 || y < 0 || x >= config.getLargura() || y >= config.getAltura())
            return null;

        for (Entidade en : entidadeGerenciador.getEntidades()) {
            ex = (int) en.x / config.getLarguraTile();
            ey = (int) en.y / config.getAlturaTile();
            if (ex == x && ey == y) {
                return en;
            }
        }

        return null;

    }

    public MapaConfig getConfig() {
        return config;
    }

    public int[] getxSpawn() {
        return xSpawn;
    }

    public int[] getySpawn() {
        return ySpawn;
    }

    public int getWidth() {
        return config.getLargura();
    }

    public int getHeight() {
        return config.getAltura();
    }

    public EntidadeGerenciador getEntidadeGerenciador() {
        return entidadeGerenciador;
    }
}
