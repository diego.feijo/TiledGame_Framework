package jTiled2D.mapas;

import java.util.ArrayList;
import java.util.List;

import jTiled2D.entidades.Entidade;
import jTiled2D.tiles.Tileset;

public class MapaConfig {

    private List<Entidade> entidades;
    private List<Layer> layers;
    private List<MapaObjeto> objetos;
    private String versao, orientacao, ordemRenderizacao;
    private int largura, altura, larguraTile, alturaTile;
    private int idProxObjeto;
    private Tileset tileset;

    public MapaConfig() {
        this.layers = new ArrayList<>();
        this.objetos = new ArrayList<>();
    }


    public int getLargura() {
        return largura;
    }

    public void setLargura(int largura) {
        this.largura = largura;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public List<Entidade> getEntidades() {
        return entidades;
    }

    public void setEntidades(List<Entidade> entidades) {
        this.entidades = entidades;
    }

    public String getVersao() {
        return versao;
    }

    public void setVersao(String versao) {
        this.versao = versao;
    }

    public String getOrientacao() {
        return orientacao;
    }

    public void setOrientacao(String orientacao) {
        this.orientacao = orientacao;
    }

    public String getOrdemRenderizacao() {
        return ordemRenderizacao;
    }

    public void setOrdemRenderizacao(String ordemRenderizacao) {
        this.ordemRenderizacao = ordemRenderizacao;
    }

    public int getLarguraTile() {
        return larguraTile;
    }

    public void setLarguraTile(int larguraTile) {
        this.larguraTile = larguraTile;
    }

    public int getAlturaTile() {
        return alturaTile;
    }

    public void setAlturaTile(int alturaTile) {
        this.alturaTile = alturaTile;
    }

    public int getIdProxObjeto() {
        return idProxObjeto;
    }

    public void setIdProxObjeto(int idProxObjeto) {
        this.idProxObjeto = idProxObjeto;
    }

    public List<Layer> getLayers() {
        return layers;
    }

    public void addLayer(Layer l) {
        this.layers.add(l);
    }

    public Tileset getTileset() {
        return tileset;
    }

    public void setTileset(Tileset tileset) {
        this.tileset = tileset;
    }

    /**
     * Localiza o objeto pelo seu nome
     */
    public List<MapaObjeto> getObjetoPorNome(String nome) {
        List<MapaObjeto> mos = new ArrayList<>();
        for (MapaObjeto mo : objetos) {
            if (mo.getNome().equalsIgnoreCase(nome)) {
                mos.add(mo);
            }
        }
        return mos;
    }

    public void adicionaObjeto(MapaObjeto mo) {
        this.objetos.add(mo);
    }



}
