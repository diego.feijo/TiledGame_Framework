/*
 This is the core class.
 Here we call the display, the assets and control the game loop.
 */
package jTiled2D;

import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import jTiled2D.gfx.GameCamera;
import jTiled2D.estados.Estado;

public class Game implements Runnable {

    private Display display;

    private boolean rodando = false;
    private Thread thread;

    private BufferStrategy bs;
    private Graphics graphic;

    private Estado estadoInicial;

    private GameCamera gameCamera;

    /**
     * Classe principal do jogo, inicializa os objetos principais.
     *
     * @param estadoInicial o estado inicial
     * @throws IllegalArgumentException se algum parametro for nulo
     */
    public Game(Estado estadoInicial) throws IllegalArgumentException {
        //Exceptions
        if (estadoInicial == null) throw new IllegalArgumentException();
        this.estadoInicial = estadoInicial;
    }

    private void init(){
        Config cfg = Config.getInstance();

        display = new Display(cfg.getNomeJogo(),  cfg.getLarguraJanela(), cfg.getAlturaJanela());
        display.getFrame().addKeyListener(cfg.getKeyManager());
        display.getFrame().addMouseListener(cfg.getMouseManager());
        display.getFrame().addMouseMotionListener(cfg.getMouseManager());
        display.getCanvas().addMouseListener(cfg.getMouseManager());
        display.getCanvas().addMouseMotionListener(cfg.getMouseManager());
        cfg.setGame(this);
        gameCamera = new GameCamera(0,0);
        cfg.setGameCamera(gameCamera);
        Estado.setEstado(estadoInicial);
    }

    /**
     * O ciclo do jogo
     */
    private void tick() {
        Config.getInstance().getKeyManager().tick();

        if (Estado.getEstadoAtual() != null) {
            Estado.getEstadoAtual().tick();
        }
    }

    /**
     * Renderiza os graficos na tela
     */
    private void render() {
        bs = display.getCanvas().getBufferStrategy();
        //Se ainda nao tem um BufferStrategy, cria
        if (bs == null) {
            display.getCanvas().createBufferStrategy(3);
            return;
        }
        graphic = bs.getDrawGraphics();
        //Limpa a tela
        graphic.clearRect(0, 0, Config.getInstance().getLarguraJanela(), Config.getInstance().getAlturaJanela());
        //Desenha
        if (Estado.getEstadoAtual()!= null) {
            Estado.getEstadoAtual().render(graphic);
        }
        //Finaliza
        bs.show();
        graphic.dispose();
    }

    @Override
    public void run() {
        init();
        try {            
            if (Config.getInstance().getFps() <= 0)
                throw new Exception("FPS deve ser maior que zero.");
            //garante que o jogo roda sempre ao FPS da config
            double timePerTick = 1000000000 / Config.getInstance().getFps(); //1*10^9 ticks por segundo / 60
            double delta = 0;
            long now;
            long lastTime = System.nanoTime();
            //contador de fps
            long timer = 0;

            while (rodando) {
                now = System.nanoTime();
                delta += (now - lastTime) / timePerTick;
                timer += now - lastTime;
                lastTime = now;

                //will only refresh if reached the fps setted
                if (delta >= 1) {
                    tick();
                    render();
                    delta--;
                }
                if (timer >= 1000000000) {
                    timer = 0;
                }
            }
            stop();
        } 
        catch (Exception ex){
            ex.printStackTrace();
        }
    }

    //inicia a thread do jogo
    public synchronized void start() {
        if (rodando) {
            return;
        }
        rodando = true;
        thread = new Thread(this);
        thread.start();
    }

    //para a thread do jogo
    public synchronized void stop() {
        if (!rodando) {
            return;
        }
        rodando = false;
        try {
            thread.join();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public Estado getEstadoInicial() {
        return estadoInicial;
    }

    public Display getDisplay() {
        return display;
    }

    public Graphics getGraphic() {
        return graphic;
    }

    public void setEstadoInicial(Estado estadoInicial) {
        this.estadoInicial = estadoInicial;
    }

}
