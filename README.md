# JTiled2D
## A Java library to game devs

One day my teacher said "You are going to make a game in Java so I can grade your face" and then I cried.
Next morning I've told myself that I could do it. And then I did. And it was garbage. 
It took me so much time to learn the basics of game dev — as an aspiring Java dev — that in the end my game had no content. You could walk back and forth and eventually cause a null reference when the player went out of bounds.

And it all started!

I was learning Java through game dev! A dream came true!

So I've compiled all my work, cleaned it a little, applied some design patterns and good practices and JTIled2D was born.

And my face was graded. 
A good grade. 


Now I'm able to share my work with you folks. Maybe it will help someone to find an inner game dev passion as I did.

### What is JTiled2D?
* A library to support an aspiring Java game dev to develop a top down, two dimensional game.
* An entry level drug for game dev future addicts.
* Something you will use for some time and then realize there are better tools that can help you to build your dreams.
* Not garbage.

### What JTiled2D is not?
* A tool to develop professional games. Seriously, there's so many engines, libraries and frameworks out there to do this. JTiled2D is not trying to compete. Is just another way to learn the basics of game dev in Java.
* Garbage.
 
### Features

#### [Tiled](https://www.mapeditor.org/) support
JTiled2D has a native Tiled map importer. You can also build your own importer!

#### Game Config
An easy way to set your game configuration as screen size, tile size, windowskin, font, font color and a lot more. Game Config is always ready to use and always on hand.

#### Camera control 
The game camera always follows the player. Not out of bounds. No null reference.

#### Entity manager 
You can fill your game with players, npcs, enemies, coins, treasure chests, doors and whatever you can imagine as an interactive entity.

#### State Manager
Your game has a menu? Nice!
When you press start, a map appears? Awesome!
Wait, ESC pops up another menu? OMG so clever!
Yes, you can do it.
With State Manager is easy to transition between states. Map after map, scene after scene.

#### Keyboard and Mouse input
You can easily set so your player move when you press arrow keys, or jump on spacebar, or say something funny with ctrl. It is an _if_ away :).

#### Always growing
JTiled2D is in a constant cycle of bug fixing. Also always adding new features on the roadmap. 

## Download the newest version
[Download here](https://gitlab.com/diego.feijo/TiledGame_Framework/-/tree/master/out/artifacts/jTiled2D_jar) 

